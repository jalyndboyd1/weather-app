import "./App.css";
import Weather from "./Weather";
import "bootstrap/dist/css/bootstrap.min.css";
import "weather-icons/css/weather-icons.css";
import React, { useState, useEffect } from "react";

const API_KEY = "b8a29e94b69c3063cd5ea7ce32816e01";
// api.openweathermap.org/data/2.5/weather?q=London,uk&appid={API key}
function App() {
  useEffect(() => {
    fetch(
      `api.openweathermap.org/data/2.5/weather?q=London,uk&appid=${API_KEY}`
    ).then((response) => console.log(response));
  }, []);

  return (
    <div className="App">
      <Weather />
    </div>
  );
}
export default App;
